#!/usr/bin/env python
#robot.py implementation goes here
import rospy
from cse_190_assi_3.msg import AStarPath as ASterPath
from cse_190_assi_3.msg import PolicyList
from astar import ASterRun

from std_msgs.msg import Bool#, String, Float32

from read_config import read_config
from mdp import runner


class robot():	
	def __init__(self, runTests):
		if(runTests):
			return
		self.config = read_config()
		moveList = self.config['move_list']
		mapSize = self.config['map_size']
		start = self.config['start']
		goal = self.config['goal']
		walls = self.config['walls']
		pits = self.config['pits']
		rospy.init_node("robot")
		mdpPub = rospy.Publisher(
			"/results/policy_list",
			PolicyList,
			queue_size = 10
		)
		ASterPub = rospy.Publisher(
			"/results/path_list",
			ASterPath,
			queue_size = 10
		)
		donePub = rospy.Publisher(
			"/map_node/sim_complete",
			Bool,
			queue_size = 10
		)
		rospy.sleep(2);
		#do A*
		dataList = ASterRun(moveList, mapSize, start, goal, walls, pits )
		for data in dataList:
			#data = dataList.get()
			message = ASterPath()
			message.data = data 
			ASterPub.publish(message)
			rospy.sleep(0.1)
		#do MDP
		runner(self.config);
		message = Bool()
		message.data = True
		donePub.publish(message)
		rospy.sleep(1)
		rospy.signal_shutdown("HueHueHue")

	#TESTS START
def test():
	ms = robot(True)
if __name__ == '__main__':
	test()
	ms = robot(False)
