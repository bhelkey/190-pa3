# astar implementation needs to go here
from Queue import PriorityQueue
import copy
from copy import deepcopy
from Queue import Queue

def dist(a,b):
	return abs(a[0] - b[0]) + abs(a[1] - b[1])

def ASterRun(moveList, mapdim, start, goal, walls, pits):
	queue = PriorityQueue()
	visited = {} 
	moves = []
	moves.append(start)
	queue.put((0, (start, moves)))
	while queue:
		
		node = queue.get()
		LandH = node[0]
	 	LocAndPath = node[1]
		loc, path = LocAndPath

		if str(loc) in visited:
			continue	
		visited[str(loc)] = 42
		if(loc == goal):
			return path
		neigh = []
		for move in moveList:
			neigh.append([loc[0] + move[0], loc[1] + move[1]])

		for b in neigh:
			p = path[:]
			if(b in walls):
				continue
			if(b in pits or b[0] < 0 or b[1] < 0):		
				continue
			
			if(b[0] >= mapdim[0] or b[1] >= mapdim[1]):
				continue

			p.append(b)
			hur = dist(b, goal)
			queue.put((len(p) + hur, (b, p)))

