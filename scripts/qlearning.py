from copy import deepcopy
import copy
from cse_190_assi_3.msg import PolicyList
import rospy

# mdp implementation needs to go here
def qlearning(config):
	mdpPub = rospy.Publisher(
		"/results/policy_list",
		PolicyList,
		queue_size = 10
	)
	#import config
	moveList = config['move_list']
	mapSize = config['map_size']
	start = config['start']
	goal = config['goal']
	walls = config['walls']
	pits = config['pits']
	maxiter = config['max_iterations']
	threshold = config['threshold_difference']
	stepreward = config['reward_for_each_step']
	wallreward = config['reward_for_hitting_wall']
	goalreward = config['reward_for_reaching_goal']
	pitreward = config['reward_for_falling_in_pit']
	discount = config['discount_factor']
	forwardprob = config['prob_move_forward']
	backwardprob = config['prob_move_backward']
	leftprob = config['prob_move_left']
	rightprob = config['prob_move_right']
	locmap = [[0 for i in range(mapSize[1])] for i in range(mapSize[0])]
	policies = [[0 for i in range(mapSize[1])] for i in range(mapSize[0])]
	problist = [forwardprob,backwardprob,leftprob,rightprob]
	direction = ["E","W","S","N"]
	for pit in pits:
		locmap[pit[0]][pit[1]] = pitreward
		policies[pit[0]][pit[1]] = "PIT"
	locmap[goal[0]][goal[1]] = goalreward
	policies[goal[0]][goal[1]] = "GOAL"
	for wall in walls:
		policies[wall[0]][wall[1]] = "WALL"
 	for i in range(maxiter):
		#get a copy of array
		sum = 0
		arraycopy = copy.deepcopy(locmap)
		for x in range(mapSize[0]):
			for y in range(mapSize[1]):
				if([x,y] in walls or [x,y] in pits or [x,y] == goal):
					continue
				#make value for state
				value = -9999999999999999999999999
				for k in range(len(moveList)):
					move = moveList[k]
					newValue = 0
					newValue+= problist[0]*calcValue(x+move[0],y+move[1],x,y,config,arraycopy)
					newValue+= problist[1]*calcValue(x-move[0],y-move[1],x,y,config,arraycopy)
					newValue+= problist[2]*calcValue(x+move[1],y+move[0],x,y,config,arraycopy)
					newValue+= problist[3]*calcValue(x-move[1],y-move[0],x,y,config,arraycopy)				
					if(newValue > value):
						policies[x][y] = direction[k]
						value = newValue
				arraycopy[x][y] = value
				sum += abs(locmap[x][y]-arraycopy[x][y])
		if(sum < threshold):
			break;
		locmap = arraycopy
		sum = 0
	 	toPublish = []
		for x in range(mapSize[0]):
			for y in range(mapSize[1]):
				toPublish.append(policies[x][y])
		message = PolicyList()
		message.data = toPublish
		mdpPub.publish(message)

		while not is_shutdown():
      			print "gimme something, please?"
     			something = raw_input()
     			print "thanks for giving me " + something
	##########################PRINT HERE##########################
		for x in range(mapSize[0]):
			toPrint = ""
			for y in range(mapSize[1]):
				if([x,y] in walls):
					toPrint += "W"
				elif([x,y] in pits):
					toPrint += "P"
				elif([x,y] == goal):
					toPrint += "G"
				else:
					toPrint += " "
			print toPrint
		print "***********ITERATION: " + str(i) + "*************"
		print ""

				

def calcValue(newx,newy,x,y,config,cmap):
	moveList = config['move_list']
	mapSize = config['map_size']
	start = config['start']
	goal = config['goal']
	walls = config['walls']
	pits = config['pits']
	maxiter = config['max_iterations']
	threshold = config['threshold_difference']
	stepreward = config['reward_for_each_step']
	wallreward = config['reward_for_hitting_wall']
	goalreward = config['reward_for_reaching_goal']
	pitreward = config['reward_for_falling_in_pit']
	discount = config['discount_factor']
	forwardprob = config['prob_move_forward']
	backwardprob = config['prob_move_backward']
	leftprob = config['prob_move_left']
	rightprob = config['prob_move_right']
	if(newx >= mapSize[0] or newy >=mapSize[1] or newx < 0 or newy < 0 or [newx,newy] in walls):
		return cmap[x][y]*discount+wallreward
	return stepreward+cmap[newx][newy]*discount

