from copy import deepcopy
import copy
from cse_190_assi_3.msg import PolicyList
import rospy
import random
import tty, sys
import sys, tty, termios

usage = "USAGE:\n <w>: move the robot up \n <a>: move the robot left \n <s>: move the robot down \n <d>: move the robot right \n <q>: quit the program \n <h>: print usage/help \n <r>: auto-simulate robot 1000 steps"

def getch():
	fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

def runner(config):
	key = 'z'
	mdp = MDP(config);
	mdp.printMap()
	print usage
	while(key != 'q'):
		key = getch()
		if(key == 'w'):
			mdp.MDPFunc(config, 3, False)
		elif(key == 'a'):
			mdp.MDPFunc(config, 1, False)
		elif(key == 's'):
			mdp.MDPFunc(config, 2, False)
		elif(key == 'd'):
			mdp.MDPFunc(config, 0, False)
		elif(key == 'h'):
			print usage
		elif(key == 'r'):
			mdp.autorun(config, mdp)
		elif(key == 'q'):
			pass
		else:
			print("key not used")
	#import config
class MDP:
	def __init__(self, config):
		self.config = config
		self.moveList = config['move_list']
		self.mapSize = config['map_size']
		self.start = config['start']
		self.goal = config['goal']
		self.walls = config['walls']
		self.pits = config['pits']
		self.stepreward = config['reward_for_each_step']
		self.wallreward = config['reward_for_hitting_wall']
		self.goalreward = config['reward_for_reaching_goal']
		self.pitreward = config['reward_for_falling_in_pit']
		self.discount = config['discount_factor']
		self.forwardprob = config['prob_move_forward']
		self.backwardprob = config['prob_move_backward']
		self.leftprob = config['prob_move_left']
		self.rightprob = config['prob_move_right']
		self.locmap = [[0 for i in range(self.mapSize[1])] for i in range(self.mapSize[0])]
		self.qvalues = [[0 for i in range(self.mapSize[1])] for i in range(self.mapSize[0])]
		self.alphas = [[0 for i in range(self.mapSize[1])] for i in range(self.mapSize[0])]		
		for x in range(self.mapSize[0]):
			for y in range(self.mapSize[1]):
				self.alphas[x][y] = [1,1,1,1]
		for x in range(self.mapSize[0]):
			for y in range(self.mapSize[1]):
				self.qvalues[x][y] = [0,0,0,0]
				if [x,y] == self.goal:
					self.qvalues[x][y] = [self.goalreward,0,0,0]
				if [x,y] in self.pits:
					self.qvalues[x][y] = [self.pitreward,self.pitreward,self.pitreward,self.pitreward]
		self.problist = [self.forwardprob,self.backwardprob,self.leftprob,self.rightprob]
		self.direction = ["E","W","S","N"]
		self.robot = [self.start[0],self.start[1]]
		for pit in self.pits:
			self.locmap[pit[0]][pit[1]] = self.pitreward
		self.locmap[self.goal[0]][self.goal[1]] = self.goalreward

	def autorun(self,config, mdp):		
		probExplore = 20
		for i in range(0,1000):
			maxVal = -999999
			maxIndex = -1
			i = 0
			for a in self.qvalues[self.robot[0]][self.robot[1]]:
				if a > maxVal:
					maxVal = a
					maxIndex = i
				i = i + 1
			if random.randrange(0,100) > probExplore:
				val = maxIndex		
			else:
				val = random.randint(0,3)
			mdp.MDPFunc(config, val, True)
		mdp.printMap()
			

	def printMap(self):
		self.printBoxMap(self.qvalues, 3, True)
	
	def MDPFunc(self, config, move, isauto):
			rand = random.random()
			probRoll = [0, 0, 0, 0]
			i = 0
			for k in range(len(self.problist)):
				i += self.problist[k]
				if(rand < i):
					probRoll[k] = 1
					break

			moveLoc = self.moveList[move]
			newValue = 0

			oldx,oldy = self.robot[0],self.robot[1]
			if(probRoll[0] == 1):
				newValue+= self.calcValue(self.robot[0] + moveLoc[0],self.robot[1] + moveLoc[1], self.robot[0], self.robot[1],config, self.qvalues)
			if(probRoll[1] == 1):
				newValue+= self.calcValue(self.robot[0] - moveLoc[0],self.robot[1] - moveLoc[1], self.robot[0], self.robot[1],config, self.qvalues)
			if(probRoll[2] == 1):
				newValue+= self.calcValue(self.robot[0] + moveLoc[1],self.robot[1] + moveLoc[0], self.robot[0], self.robot[1],config, self.qvalues)
			if(probRoll[3] == 1):
				newValue+= self.calcValue(self.robot[0] - moveLoc[1],self.robot[1] - moveLoc[0], self.robot[0], self.robot[1],config, self.qvalues)				
			
			alpha = 1.0/(self.alphas[oldx][oldy][move])
			self.qvalues[oldx][oldy][move] = (1.0-alpha)*self.qvalues[oldx][oldy][move] + alpha*newValue
			self.alphas[oldx][oldy][move] += 1
			if not isauto:
				self.printMap()

					

	def calcValue(self, newx,newy,x,y,config,qmap):
		if(newx >= self.mapSize[0] or newy >= self.mapSize[1] or newx < 0 or newy < 0 or [newx,newy] in self.walls):
			return max(qmap[x][y])*self.discount+self.wallreward
		if([newx, newy] == self.goal or [newx, newy] in self.pits):
			self.robot = self.start[:]
		else:
			self.robot[0] = newx
			self.robot[1] = newy
		return self.stepreward+max(qmap[newx][newy])*self.discount



#BEGIN PRINT FXNS
	def printBoxMap(self, qValues, halfHeight, elong = False):
	    boxSize = halfHeight * 2 + 1

	    boxStart = 0
	    boxStartNext = 0
	    boxMap = []
	    for i in range(0, len(qValues)):
		for j in range(0, len(qValues[i])):
		    center = " "
		    if([i, j] == self.robot):
			center = "R"
		    box = makeBox(boxSize, center, [qValues[i][j][3], qValues[i][j][1],
					       qValues[i][j][0], qValues[i][j][2]], elong)

		    if( [i, j] in self.pits): #TODO change to detect pits
			specialName = "Pit"
			specialProb = self.locmap[i][j] 
			box = makeEmptyBox(boxSize, specialName, specialProb, elong)
		    if( [i, j] in self.walls): #TODO change to detect pits
			specialName = "Wall"
			specialProb = self.locmap[i][j] 
			box = makeEmptyBox(boxSize, specialName, specialProb, elong)
		    if( [i, j] == self.goal): #TODO change to detect pits
			specialName = "Goal"
			specialProb = self.locmap[i][j] 
			box = makeEmptyBox(boxSize, specialName, specialProb, elong)


		    box = coverBox(box, j==0, i==0)
				      
		    if(j == 0):
			for line in box:
			    boxMap.append(line)
			boxStartNext += len(box)
			continue
		    for k in range(0, len(box)):
			line = box[k]
			boxMap[boxStart + k] += line
			
		boxStart = boxStartNext
	    for line in boxMap:
		print line

def bufSpace(i):
    s = ""
    for a in range(0, i):
        s += " "
    return s

def makeEmptyBox(size, center, num, elong = False):
    box = []
    lineSize = size
    if elong:
        lineSize = lineSize * 2 - 1
    for i in range(size / 2):
        box.append(bufSpace(lineSize))

    line = bufSpace((lineSize - len(center)) / 2)
    line += center + bufSpace((lineSize - len(center)) / 2)                 
    while len(line) < lineSize:
        line += " "
    box.append(line)

    numStr = "{:^5.3f}".format(num)
    line = bufSpace((lineSize - len(numStr)) / 2)
    line += numStr + bufSpace((lineSize - len(numStr)) / 2)
    while len(line) < lineSize:
        line += " "
    box.append(line)

    for i in range(size / 2 - 1):
        box.append(bufSpace(lineSize))
    return box
    

def makeBox(size, center, numList, elong = False):
    halfLength = size / 2
    if elong:
        size = size * 2 - 1
        halfLength = size / 4
        
    
    box = []
    numStr =  "{:^5.3f}".format(numList[0])    
    line = "\\" + bufSpace((size - 2 - len(numStr)) / 2)
    line += numStr + bufSpace((size - 2 - len(numStr)) / 2)
    while len(line) < size - 1:
        line += " "
    box.append(line + "/")
    
    for i in range(1, halfLength):
        if elong:
            i = i * 2
        line = ""
        line += bufSpace(i) + "\\"
        line += bufSpace(size - 2 * i - 2) + "/"
        line += bufSpace(i)
        box.append(line)

    numStr1 = "{:<5.3f}".format(numList[1])
    numStr2 = "{:>5.3f}".format(numList[2])
    line = numStr1 + bufSpace((size - 1) / 2 - len(numStr1))
    line += center
    line += bufSpace((size - 1) / 2 - len(numStr2))
    while len(line) < size - len(numStr2):
        line += " "
    line += numStr2
    box.append(line)

    for i in reversed(range(1, halfLength)):
        if elong:
            i = i * 2
        line = ""
        line += bufSpace(i) + "/"
        line += bufSpace(size - 2 * i - 2) + "\\"
        line += bufSpace(i)
        box.append(line)

    numStr = "{:<5.3f}".format(numList[3])
    line = "/" + bufSpace((size - 2 - len(numStr)) / 2)
    line += numStr
    line += bufSpace((size -  2 - len(numStr)) / 2)

    while len(line) < size - 1:
        line += " "

    box.append(line + "\\")
    
    return box

def coverBox(box, side = False, top = False):
    endLine = ""
    for i in range(len(box[0])):
        endLine += "-"
    box = [line + "|" for line in box]
    endLine += "+"
    box.append(endLine)
    if(top):
        box = [endLine] + box
    if(side):
        box = ["|" + line for line in box]
        botLeft = list(box[len(box) - 1])
        botLeft[0] = '+'
        box[len(box) - 1] = "".join(botLeft)
    if(top and side):
        top = list(box[0])
        top[0] = '+'
        box[0] = "".join(top)
    return box
